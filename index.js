const { getClient, createKeyspaceAndTable, insertData } = require('./cassandraApp');
const cassandra = require('cassandra-driver');


const argv = process.argv.slice(2);
//const contactPoints = argv[0] || (process.env.CASSANDRA_CONTACT_POINTS ? process.env.CASSANDRA_CONTACT_POINTS.split(',') : ['127.0.0.1']);
//const localDataCenter = argv[1] || process.env.CASSANDRA_LOCAL_DC || 'datacenter1';

const contactPoints = argv[0] ? (argv[0].split(',')) : ((process.env.CASSANDRA_CONTACT_POINTS) ? (process.env.CASSANDRA_CONTACT_POINTS.split(',')) : (['127.0.0.1']));
const localDataCenter = argv[1] || (process.env.CASSANDRA_LOCAL_DC || 'datacenter1');
const keyspace = 'demo';
const table = 'random_data';

const main = async () => {
  const client = getClient(contactPoints, localDataCenter);

  // Connect to the cluster
  try {
    await client.connect();
    console.log('Connected to Cassandra');

    await createKeyspaceAndTable(client, keyspace, table);

    // Generate and insert random data every 5 seconds
    setInterval(async () => {
      const id = cassandra.types.uuid();
      const data = Math.random().toString(36).substring(2);

      await insertData(client, keyspace, table, id, data);
    }, 5000);
  } catch (err) {
    console.log(err);
  }
};

main();
