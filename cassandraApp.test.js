const { getClient, createKeyspaceAndTable, insertData } = require('./cassandraApp');
const cassandra = require('cassandra-driver');
const { v4: uuidv4 } = require('uuid');

jest.mock('cassandra-driver');

describe('Cassandra App', () => {
  beforeEach(() => {
    cassandra.Client.mockClear();
  });

  test('getClient() creates a new cassandra client with correct options', () => {
    const contactPoints = ['127.0.0.1'];
    const localDataCenter = 'datacenter1';

    getClient(contactPoints, localDataCenter);

    expect(cassandra.Client).toHaveBeenCalledWith({
      contactPoints: contactPoints,
      localDataCenter: localDataCenter
    });
  });

  test('createKeyspaceAndTable() creates keyspace and table', async () => {
    const client = new cassandra.Client({});
    client.execute = jest.fn();

    const keyspace = 'demo';
    const table = 'random_data';

    await createKeyspaceAndTable(client, keyspace, table);

    expect(client.execute).toHaveBeenCalledTimes(2);
  });

  test('insertData() inserts data into the specified table', async () => {
    const client = new cassandra.Client({});
    client.execute = jest.fn();

    const keyspace = 'demo';
    const table = 'random_data';
    const id = uuidv4();
    const data = 'random_data';

    await insertData(client, keyspace, table, id, data);

    expect(client.execute).toHaveBeenCalledTimes(1);
  });
});
