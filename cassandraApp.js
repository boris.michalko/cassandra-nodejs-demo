const cassandra = require('cassandra-driver');

const getClient = (contactPoints, localDataCenter) => {
  return new cassandra.Client({
    contactPoints: contactPoints,
    localDataCenter: localDataCenter
  });
};

const createKeyspaceAndTable = async (client, keyspace, table) => {
  try {
    const createKeyspace = `CREATE KEYSPACE IF NOT EXISTS ${keyspace} WITH replication = {'class': 'SimpleStrategy', 'replication_factor': '1' }`;
    const createTable = `CREATE TABLE IF NOT EXISTS ${keyspace}.${table} (id uuid PRIMARY KEY, data text)`;

    await client.execute(createKeyspace);
    await client.execute(createTable);
  } catch (err) {
    console.log(err);
  }
};

const insertData = async (client, keyspace, table, id, data) => {
  try {
    const query = `INSERT INTO ${keyspace}.${table} (id, data) VALUES (?, ?)`;
    await client.execute(query, [id, data], { prepare: true });
    console.log(`Inserted data: ${id}, ${data}`);
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  getClient,
  createKeyspaceAndTable,
  insertData
};
